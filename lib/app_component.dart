import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/common/constants.dart';
import 'package:visabymasters_angulardart/src/main-windows/bottom-bar/bottom_bar_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/contact-us/contact_us_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/main_windows_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/testimonials/testimonials_component.dart';
import 'package:visabymasters_angulardart/src/main_routes.dart';
import 'package:visabymasters_angulardart/src/main_routes_paths.dart';
import 'package:visabymasters_angulardart/src/services/applynow_service.dart';
import 'package:visabymasters_angulardart/src/services/country_service.dart';
import 'package:visabymasters_angulardart/src/services/visa_service.dart';
import 'package:visabymasters_angulardart/src/top-navigation/top_navigation_component.dart';
import 'package:visabymasters_angulardart/src/top-progress-bar/top_progress_component.dart';

@Component(
  selector: 'my-app',
  styleUrls: ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: [
    NgIf,
    MainWindowsComponent,
    routerDirectives,
    TopNavigationComponent,
    TopProgressComponent,
    BottomBarComponent,
    ContactUsComponent,
    TestimonialsComponent
  ],
  providers: [
    ClassProvider(CountryService),
    ClassProvider(VisaService),
    ClassProvider(ApplyNowService)
  ],
  exports: [MainRoutes, MainRoutePaths],
)
class AppComponent implements OnInit {
  final VisaService visaService;
  final CountryService _countryService;
  final Router _router;
  static bool isShowProgressBar = false;

  AppComponent(this.visaService, this._countryService, this._router);

  @override
  void ngOnInit() {
    _router.onRouteActivated.listen((onData) => {
          if (onData.routePath.path == Constants.home)
            {isShowProgressBar = true}
        });

    if (!_countryService.isLoaded) {
      _countryService.getAllCountries();
    }
  }
}
