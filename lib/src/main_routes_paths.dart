import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/common/constants.dart';

const homeCountryId = 'id';
const foreignCountryId = 'fId';

class MainRoutePaths {
  static final main = RoutePath(path: Constants.mainpage);
  static final signin = RoutePath(path: Constants.signin);
  static final visabycountry = RoutePath(
    path: '${Constants.visabycountry}/:$homeCountryId/:$foreignCountryId',
  );
  static final adminpanel = RoutePath(path: Constants.adminpanel);
  static final applyNow = RoutePath(path: Constants.applyNow);
}

int getHomeId(Map<String, String> parameters) {
  final id = parameters[homeCountryId];
  return id == null ? null : int.parse(id);
}

int getForeignId(Map<String, String> parameters) {
  final id = parameters[foreignCountryId];
  return id == null ? null : int.parse(id);
}
