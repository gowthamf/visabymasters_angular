@JS()
library t;

import 'package:js/js.dart';

@JS()
external int Test();


class JSTest{
  JSTest();

  int get value => Test();
}