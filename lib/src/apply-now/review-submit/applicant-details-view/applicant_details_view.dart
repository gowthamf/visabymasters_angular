import 'package:angular/angular.dart';
import 'package:visabymasters_angulardart/src/models/applicant.dart';

@Component(
    selector: 'applicant-details-view',
    templateUrl: 'applicant_details_view.html',
    styleUrls: ['applicant_details_view.css'])
class ApplicantDetailsView {
  @Input()
  Applicant applicant;

  String fName =
      "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
  String lName =
      "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
  String gender = "Male";
  String cob = "Sri Lanka";
  String dob = "19 Jun 1996";
  String nat = "Sri Lankan";
  String pptNo = "N7825652";
  String pptExp = "31 Sep 2028";
  String pptIss = "31 Sep 2018";
}
