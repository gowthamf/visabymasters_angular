import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:visabymasters_angulardart/src/apply-now/review-submit/applicant-details-view/applicant_details_view.dart';
import 'package:visabymasters_angulardart/src/models/CountryDTO.dart';
import 'package:visabymasters_angulardart/src/models/applicant.dart';
import 'package:visabymasters_angulardart/src/services/applynow_service.dart';

@Component(
    selector: 'review-submit-component',
    templateUrl: 'review_submit_component.html',
    styleUrls: [
      'review_submit_component.css',
      'package:angular_components/css/mdc_web/card/mdc-card.scss.css'
    ],
    directives: [
      ApplicantDetailsView,
      MaterialButtonComponent,
      MaterialIconComponent,
      NgIf
    ])
class ReviewSubmitComponent implements OnInit {
  int index = 1;
  bool isShowPreviousBtn;
  bool isShowNextBtn;

  final ApplyNowService applyNowService;
  Applicant applicant;

  ReviewSubmitComponent(this.applyNowService);

  void viewNext() {
    if (index >= 1) {
      index = index + 1;
      applicant = applyNowService.applicantList.elementAt(index - 1);
      isShowPreviousBtn = false;
    }
    if (index == applyNowService.applicantList.length) {
      isShowNextBtn = true;
      isShowPreviousBtn = false;
    }
  }

  void viewPrevious() {
    if (index > 1) {
      index = index - 1;
      applicant = applyNowService.applicantList.elementAt(index - 1);
      isShowNextBtn = false;
    }
    if (index == applyNowService.applicantList.length) {
      isShowPreviousBtn = true;
      isShowNextBtn = false;
    }
    if (index == 1) {
      isShowPreviousBtn = true;
    }
  }

  @override
  void ngOnInit() {
    if (applyNowService.visaType.country == null) {
      applyNowService.visaType.country = CountryDTO();
    }
    applicant = applyNowService.applicantList.elementAt(index - 1);

    if (index == 1 && applyNowService.applicantList.length == 1) {
      isShowNextBtn = true;
      isShowPreviousBtn = true;
    } else {
      isShowNextBtn = false;
      isShowPreviousBtn = true;
    }
  }
}
