import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/material_input/material_input_auto_select.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:visabymasters_angulardart/src/common/dropdown-list/dropdown_list_component.dart';
import 'package:visabymasters_angulardart/src/models/CountryDTO.dart';
import 'package:visabymasters_angulardart/src/models/applicant.dart';

@Component(
    selector: 'applicant-details-template',
    templateUrl: 'applicant_details_template.html',
    styleUrls: [
      'applicant_details_template.css',
      'package:angular_components/css/mdc_web/card/mdc-card.scss.css'
    ],
    directives: [
      DeferredContentDirective,
      MaterialButtonComponent,
      MaterialDropdownSelectComponent,
      MaterialPopupComponent,
      MaterialTooltipDirective,
      PopupSourceDirective,
      MaterialFabComponent,
      MaterialIconComponent,
      formDirectives,
      AutoFocusDirective,
      MaterialButtonComponent,
      MaterialIconComponent,
      MaterialInputComponent,
      materialInputDirectives,
      MaterialMultilineInputComponent,
      MaterialInputAutoSelectDirective,
      materialNumberInputDirectives,
      MaterialPaperTooltipComponent,
      MaterialTooltipTargetDirective,
      MaterialButtonComponent,
      MaterialDatepickerComponent,
      DateRangeInputComponent,
      CountyDropdownListComponent,
      MaterialTooltipDirective,
      MaterialTooltipTargetDirective,
      MaterialTooltipSourceDirective
    ])
class ApplicantDetailsTemplate implements OnInit {
  @Input()
  Applicant applicant;

  final values = ["Male", "Female", "Not Specify"];
  static const String dob = "Date Of Birth";
  static const String ppExp = "Passport Expiry Date";
  static const String ppIss = "Passport Issued Date";
  static const String selectedCountry = "Nationality";
  static const String selectBC = "Country of Birth";

  @override
  void ngOnInit() {
    applicant.gender = "Gender";
    applicant.nationality = "Nationality";
  }

  void getSelectedNationality(CountryDTO countryDTO) {
    applicant.nationality = countryDTO.countryName;
  }

  void getCountryOfBirth(CountryDTO countryDTO) {
    applicant.cOfBirth = countryDTO.countryName;
  }
}
