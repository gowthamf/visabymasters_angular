import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:visabymasters_angulardart/src/apply-now/applicant_details/applicant-details-template/applicant_details_template.dart';
import 'package:visabymasters_angulardart/src/models/applicant.dart';
import 'package:visabymasters_angulardart/src/services/applynow_service.dart';

@Component(
    selector: 'applicant-details-component',
    templateUrl: 'applicant_details_component.html',
    styleUrls: [
      'applicant_details_component.css'
    ],
    directives: [
      ApplicantDetailsTemplate,
      MaterialButtonComponent,
      NgFor,
      NgIf,
      formDirectives
    ])
class ApplicantDetailsComponent {
  final ApplyNowService applyNowService;

  ApplicantDetailsComponent(this.applyNowService);

  void addMoreApplicant() {
    applyNowService.applicantList.add(Applicant());
  }
}
