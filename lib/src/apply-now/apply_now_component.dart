import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/utils/angular/scroll_host/angular_2.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/apply-now/applicant_details/applicant_details_component.dart';
import 'package:visabymasters_angulardart/src/apply-now/general-information/general_information_component.dart';
import 'package:visabymasters_angulardart/src/apply-now/review-submit/review_submit_component.dart';
import 'package:visabymasters_angulardart/src/apply-now/visa-type/visa_type_component.dart';
import 'package:visabymasters_angulardart/src/common/custom-stepper/custom_step.dart';
import 'package:visabymasters_angulardart/src/common/custom-stepper/custom_step.dart'
    as prefix;
import 'package:visabymasters_angulardart/src/common/custom-stepper/custom_stepper.dart';
import 'package:visabymasters_angulardart/src/models/general_info.dart';
import 'package:visabymasters_angulardart/src/services/applynow_service.dart';
import 'package:visabymasters_angulardart/src/services/visa_service.dart';

@Component(
    selector: 'apply-now-component',
    providers: [scrollHostProviders, materialProviders],
    templateUrl: 'apply_now_component.html',
    styleUrls: ['apply_now_component.css'],
    directives: [
      CustomMaterialStepperComponent,
      CustomStepDirective,
      prefix.CustomSummaryDirective,
      MaterialButtonComponent,
      NgFor,
      GeneralInformationComponent,
      VisaTypeComponent,
      ApplicantDetailsComponent,
      ReviewSubmitComponent
    ])
class ApplyNowComponent implements OnInit {
  final VisaService _visaService;
  final ApplyNowService _applyNowService;

  @Input()
  String data;

  ApplyNowComponent(this._visaService, this._applyNowService);

  void submittingApplication(dynamic applicationDetails) {
    print(_applyNowService.generalInfo.emailAddress);
    print(applicationDetails);
  }

  @override
  void ngOnInit() {
    _visaService.isVisaCountry = false;
  }

  void getDetails(GeneralInfo generalInfo) {
    //print(generalInfo.emailAddress);
  }
}
