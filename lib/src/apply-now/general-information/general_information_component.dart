import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/laminate/overlay/zindexer.dart';
import 'package:angular_components/material_input/material_input_auto_select.dart';
import 'package:angular_components/utils/browser/window/module.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:visabymasters_angulardart/src/models/general_info.dart';
import 'package:visabymasters_angulardart/src/services/applynow_service.dart';

@Component(
    selector: 'general-information-component',
    providers: [
      popupBindings,
      ClassProvider(ZIndexer),
      windowBindings,
      datepickerBindings
    ],
    templateUrl: 'general_information_component.html',
    styleUrls: ['general_information_component.css'],
    directives: [
      DeferredContentDirective,
      MaterialButtonComponent,
      MaterialDropdownSelectComponent,
      MaterialPopupComponent,
      MaterialTooltipDirective,
      PopupSourceDirective,
      MaterialFabComponent,
      MaterialIconComponent,
      formDirectives,
      AutoFocusDirective,
      MaterialButtonComponent,
      MaterialIconComponent,
      MaterialInputComponent,
      materialInputDirectives,
      MaterialMultilineInputComponent,
      MaterialInputAutoSelectDirective,
      materialNumberInputDirectives,
      MaterialPaperTooltipComponent,
      MaterialTooltipTargetDirective,
      MaterialButtonComponent,
      MaterialDatepickerComponent,
      DateRangeInputComponent,
      MaterialTooltipDirective,
      MaterialTooltipTargetDirective,
      MaterialTooltipSourceDirective
    ])
class GeneralInformationComponent implements OnInit {
  static const String intendedDepDate = "Depature Date";
  static const String intenedArrivalDate = "Arrival Date";
  Date date;

  final ApplyNowService applyNowService;

  GeneralInformationComponent(this.applyNowService);

  @override
  void ngOnInit() {}
}
