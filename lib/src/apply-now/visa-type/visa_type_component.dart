import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:visabymasters_angulardart/src/common/dropdown-list/dropdown_list_component.dart';
import 'package:visabymasters_angulardart/src/models/CountryDTO.dart';
import 'package:visabymasters_angulardart/src/models/optionalService.dart';
import 'package:visabymasters_angulardart/src/models/visaType.dart';
import 'package:visabymasters_angulardart/src/services/applynow_service.dart';

@Component(
    selector: 'visa-type-component',
    templateUrl: 'visa_type_component.html',
    styleUrls: [
      'visa_type_component.css'
    ],
    directives: [
      MaterialRadioComponent,
      CountyDropdownListComponent,
      NgFor,
      NgFormControl,
      NgModel,
      MaterialIconComponent,
      MaterialRadioGroupComponent,
      MaterialCheckboxComponent
    ])
class VisaTypeComponent {
  bool isToCountry = true;

  static const String selectedCountry = "Select Country";
  final ApplyNowService applyNowService;

  List<VisaType> visaTypes = [
    VisaType(1, "Tourist Visa"),
    VisaType(2, "Business Visa")
  ];

  List<OptionalServices> optionalServices = [
    OptionalServices(1, "Full Service", true),
    OptionalServices(2, "Form Filling Only", false)
  ];

  VisaTypeComponent(this.applyNowService) {
    applyNowService.visaType.applyVisaType = visaTypes.first;
    applyNowService.visaType.optionalService = optionalServices.first;
  }

  void getVisitingCountry(CountryDTO country) {
    applyNowService.visaType.country = country;
  }

  void selectedVisaTypeChange(dynamic visaType) {}
}
