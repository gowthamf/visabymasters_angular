import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';

@Component(
  selector: 'visa-details-component',
  providers: [overlayBindings],
  templateUrl: 'visa_details_component.html',
  directives: [
    AutoFocusDirective,
    FocusListDirective,
    MaterialIconComponent,
    MaterialButtonComponent,
    MaterialExpansionPanel,
    MaterialExpansionPanelAutoDismiss,
    MaterialExpansionPanelSet,
    MaterialDialogComponent,
    MaterialInputComponent,
    materialInputDirectives,
    MaterialYesNoButtonsComponent,
    ModalComponent,
    NgFor,
    NgModel,
  ],
  preserveWhitespace: true,
)
class VisaDetailsComponent implements OnInit {
  @Input()
  VisaDetails visaDetails;

  @override
  void ngOnInit() {}
}
