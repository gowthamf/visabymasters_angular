import 'package:angular/angular.dart';

@Component(
    selector: 'how-to-apply-component',
    templateUrl: 'how_to_apply_component.html',
    styleUrls: [
      'package:angular_components/css/mdc_web/card/mdc-card.scss.css'
    ])
class HowToApplyComponent {
  var name = 'Angular';
}
