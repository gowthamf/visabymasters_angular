import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'package:visabymasters_angulardart/src/common/dropdown-list/dropdown_list_component.dart';
import 'package:visabymasters_angulardart/src/main_routes_paths.dart';
import 'package:visabymasters_angulardart/src/services/visa_service.dart';
import 'package:visabymasters_angulardart/src/visa_by_country/on-arrival/on_arrival_component.dart';
import 'package:visabymasters_angulardart/src/visa_by_country/visa-details/visa_details_component.dart';
import 'package:visabymasters_angulardart/src/visa_by_country/visa-free/visa_free_component.dart';
import 'package:visabymasters_angulardart/src/visa_by_country/visa-required/visa_required_component.dart';

@Component(
    selector: 'visa-by-country-component',
    templateUrl: 'visa_by_country_component.html',
    directives: [
      coreDirectives,
      CountyDropdownListComponent,
      VisaRequiredComponent,
      OnArrivalComponent,
      VisaFreeComponent,
      VisaDetailsComponent
    ])
class VisaByCountryComponent implements CanActivate, OnDeactivate, OnInit {
  var name = 'Angular';
  bool isToCountry = true;
  VisaService _visaService;
  Visa visa = Visa();
  VisaDetails visaDetails = VisaDetails();
  ChangeDetectorRef _changeDetectorRef;
  bool isLoaded = false;

  VisaByCountryComponent(this._visaService, this._changeDetectorRef);

  @override
  void onDeactivate(RouterState current, RouterState next) {}

  @override
  void ngOnInit() {}

  @override
  Future<bool> canActivate(RouterState current, RouterState next) async {
    _visaService.fromCountryId = getHomeId(current.parameters);
    _visaService.toCountryId = getForeignId(current.parameters);
    window.scrollTo(0, 0);
    _visaService.getVisaRequirements(
        _visaService.fromCountryId, _visaService.toCountryId);

    visaBloc.visaRequirements.listen((onData) => {
          visa = onData,
          _visaService.getVisaRequirementsDetails(visa.visaId),
        });

    visaDetailsBloc.visaDetails.listen((onData) => {
          visaDetails = onData,
          isLoaded = true,
          _changeDetectorRef.markForCheck(),
        });

    _visaService.isVisaCountry = true;

    return true;
  }
}
