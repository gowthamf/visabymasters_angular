import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:visabymasters_angulardart/src/visa_by_country/how-to-apply/how_to_apply_component.dart';
import 'package:visabymasters_angulardart/src/visa_by_country/visa-details/visa_details_component.dart';

@Component(
    selector: 'visa-required-component',
    templateUrl: 'visa_required_component.html',
    styleUrls: [],
    preserveWhitespace: true,
    directives: [MaterialButtonComponent,HowToApplyComponent,VisaDetailsComponent])
class VisaRequiredComponent {
  String name = "Embassy Contact Info";

  void trr() {
    print('Clicked');
  }
}
