import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/laminate/overlay/zindexer.dart';
import 'package:angular_components/material_input/material_input_auto_select.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/common/constants.dart';
import 'package:visabymasters_angulardart/src/services/visa_service.dart';

@Component(
    selector: 'sign-in-component',
    templateUrl: 'sign_in_component.html',
    styleUrls: [
      'sign_in_component.css'
    ],
    providers: [
      popupBindings,
      ClassProvider(ZIndexer)
    ],
    directives: [
      DeferredContentDirective,
      MaterialButtonComponent,
      MaterialDropdownSelectComponent,
      MaterialPopupComponent,
      MaterialTooltipDirective,
      PopupSourceDirective,
      MaterialFabComponent,
      MaterialIconComponent,
      formDirectives,
      AutoFocusDirective,
      MaterialButtonComponent,
      MaterialIconComponent,
      materialInputDirectives,
      MaterialMultilineInputComponent,
      MaterialInputAutoSelectDirective,
      materialNumberInputDirectives,
      MaterialPaperTooltipComponent,
      MaterialTooltipTargetDirective,
      MaterialButtonComponent,
      VisaService,
      MaterialButtonComponent
    ])
class SignInComponent implements OnActivate {
  final VisaService _visaService;
  final Router _router;

  SignInComponent(this._visaService, this._router);

  @override
  void onActivate(RouterState previous, RouterState current) {
    _visaService.isVisaCountry = false;
  }

  void loginToAdmin() {
    _router.navigateByUrl(Constants.adminpanel, reload: true);
  }
}
