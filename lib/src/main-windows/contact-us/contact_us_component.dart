import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/laminate/overlay/zindexer.dart';
import 'package:angular_components/material_input/material_input_auto_select.dart';
import 'package:angular_forms/angular_forms.dart';

@Component(
  selector: 'contact-us-component',
  styleUrls: ['contact_us_component.css'],
  templateUrl: 'contact_us_component.html',
  providers: [popupBindings, ClassProvider(ZIndexer)],
  directives: [
    DefaultPopupSizeProvider,
    DeferredContentDirective,
    DontUseRepositionLoopProvider,
    MaterialDropdownSelectComponent,
    MaterialPopupComponent,
    MaterialTooltipDirective,
    PopupSourceDirective,
    MaterialFabComponent,
    MaterialIconComponent,
    formDirectives,
    AutoFocusDirective,
    MaterialInputComponent,
    materialInputDirectives,
    MaterialMultilineInputComponent,
    MaterialInputAutoSelectDirective,
    materialNumberInputDirectives,
    MaterialPaperTooltipComponent,
    MaterialTooltipTargetDirective,
    MaterialButtonComponent,
    ClickableTooltipTargetDirective,
    MaterialTooltipSourceDirective
  ],
)
class ContactUsComponent {
  bool visible = false;
  num numericValue = 0;
  String emailAddress = "";
  String inquiry = "";
  RelativePosition position = RelativePosition.OffsetTopLeft;
  List<RelativePosition> toolTipPositions = [RelativePosition.AdjacentTop];
  static const contactUsToolTip = "Contact Us";
}

@Injectable()
PopupSizeProvider createPopupSizeProvider() {
  return PercentagePopupSizeProvider();
}

@Directive(
  selector: '[defaultPopupSizeProvider]',
  providers: [FactoryProvider(PopupSizeProvider, createPopupSizeProvider)],
)
class DefaultPopupSizeProvider {}

@Directive(
  selector: '[dontUseRepositionLoop]',
  providers: [
    popupBindings,
    ValueProvider.forToken(overlayRepositionLoop, false),
  ],
)
class DontUseRepositionLoopProvider {}
