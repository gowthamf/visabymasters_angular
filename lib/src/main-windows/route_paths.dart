import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/common/constants.dart';
import '../main_routes_paths.dart' as _parent;



class RoutePaths {
  static final home =
      RoutePath(path: Constants.home, parent: _parent.MainRoutePaths.main);
  static final visa =
      RoutePath(path: Constants.visas, parent: _parent.MainRoutePaths.main);
  static final passports =
      RoutePath(path: Constants.passports, parent: _parent.MainRoutePaths.main);
  static final legalization = RoutePath(
      path: Constants.legalization, parent: _parent.MainRoutePaths.main);
  static final servicedirectory = RoutePath(
      path: Constants.servicedirectory, parent: _parent.MainRoutePaths.main);
  static final processstatus = RoutePath(
      path: Constants.processstatus, parent: _parent.MainRoutePaths.main);
}


