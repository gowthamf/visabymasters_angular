import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

@Component(
    selector: 'testimonial-template-component',
    templateUrl: 'testimonial_template_component.html',
    directives: [MaterialIconComponent])
class TestimonialTemplateComponent {
  String iconName='star';
}
