import 'package:angular/angular.dart';
import 'package:visabymasters_angulardart/src/main-windows/testimonials/testimonials-template/testimonial_template_component.dart';

@Component(
    selector: 'testimonials-component',
    styleUrls: ['testimonials_component.css'],
    templateUrl: 'testimonials_component.html',
    directives: [TestimonialTemplateComponent])
class TestimonialsComponent {
  var name = 'Testimonials';
}
