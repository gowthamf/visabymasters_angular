import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

@Component(
    selector: 'why-choose-us-component',
    styleUrls: [
      'why_choose_us_component.css',
      'package:angular_components/css/mdc_web/card/mdc-card.scss.css'
    ],
    templateUrl: 'why_choose_us_component.html',
    directives: [MaterialIconComponent])
class WhyChooseUsComponent {
  var name = 'Why Choose Us';
}
