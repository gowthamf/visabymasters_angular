import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

@Component(
    selector: 'bottom-bar-component',
    styleUrls: ['bottom_bar_component.css'],
    templateUrl: 'bottom_bar_component.html',
    directives: [MaterialIconComponent, MaterialButtonComponent])
class BottomBarComponent {}
