import 'package:angular_router/angular_router.dart';

import 'route_paths.dart';

import 'home/home_component.template.dart' as home_template;
import 'visas/visa_component.template.dart' as visa_template;
import 'passports/passport_component.template.dart' as passport_template;
import 'legalization/legalization_component.template.dart'
    as legalization_template;
import 'service-directory/service_directory_component.template.dart'
    as service_directory_template;
import 'process-status/process_status_component.template.dart'
    as process_status_template;
import 'not_found_component.template.dart' as not_found_template;


export 'route_paths.dart';

class Routes {
  static final homepage = RouteDefinition(
    routePath: RoutePaths.home,
    component: home_template.HomeComponentNgFactory,
  );

  static final visapage = RouteDefinition(
    routePath: RoutePaths.visa,
    component: visa_template.VisaComponentNgFactory,
  );

  static final passportspage = RouteDefinition(
    routePath: RoutePaths.passports,
    component: passport_template.PassportComponentNgFactory,
  );

  static final legalizationpage = RouteDefinition(
    routePath: RoutePaths.legalization,
    component: legalization_template.LegalizationComponentNgFactory,
  );

  static final servicedirpage = RouteDefinition(
    routePath: RoutePaths.servicedirectory,
    component: service_directory_template.ServiceDirectoryComponentNgFactory,
  );

  static final processpage = RouteDefinition(
    routePath: RoutePaths.processstatus,
    component: process_status_template.ProcessStatusComponentNgFactory,
  );

  

  static final all = <RouteDefinition>[
    homepage,
    visapage,
    passportspage,
    legalizationpage,
    servicedirpage,
    processpage,
    // visabycountry,
    RouteDefinition.redirect(
      path: '',
      redirectTo: RoutePaths.home.toUrl(),
    ),
    RouteDefinition(
      path: '.+',
      component: not_found_template.NotFoundComponentNgFactory,
    ),
  ];
}
