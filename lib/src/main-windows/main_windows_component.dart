import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/main-windows/bottom-bar/bottom_bar_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/contact-us/contact_us_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/home/home_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/route_paths.dart';
import 'package:visabymasters_angulardart/src/main-windows/routes.dart';
import 'package:visabymasters_angulardart/src/main-windows/tab-navigation/tab_navigation_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/testimonials/testimonials_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/visa-types/visa_types_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/why-choose-us/why_choose_us_component.dart';
import 'package:visabymasters_angulardart/src/services/country_service.dart';
import 'package:visabymasters_angulardart/src/services/visa_service.dart';
import 'package:visabymasters_angulardart/src/top-navigation/top_navigation_component.dart';

@Component(
  selector: 'main-windows-component',
  templateUrl: 'main_windows_component.html',
  directives: [
    HomeComponent,
    VisaTypesComponent,
    WhyChooseUsComponent,
    TopNavigationComponent,
    TabNavigationComponent,
    NgIf,
    routerDirectives,
    CountryService,
    VisaService
  ],
  exports: [RoutePaths, Routes],
)
class MainWindowsComponent implements OnInit, OnActivate {
  final CountryService _countryService;
  final VisaService _visaService;

  MainWindowsComponent(this._countryService, this._visaService);
  bool isShowVisa = true;

  void clicked(int nume) {
    print(nume);
  }

  @override
  void ngOnInit() {
    isShowVisa = _countryService.isShowVisa;
  }

  @override
  void onActivate(RouterState previous, RouterState current) {
    _visaService.isVisaCountry = true;
  }
}
