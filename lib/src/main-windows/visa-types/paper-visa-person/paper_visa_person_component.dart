import 'package:angular/angular.dart';

@Component(
  selector: 'paper-visa-person-component',
  styleUrls: ['paper_visa_person_component.css'],
  templateUrl: 'paper_visa_person_component.html',
)
class PaperVisaPersonComponent {
  var name = 'Paper Visa Person';
}
