import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:visabymasters_angulardart/src/main-windows/visa-types/electronic-visa/electronic_visa_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/visa-types/paper-visa-counter/paper_visa_counter_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/visa-types/paper-visa-person/paper_visa_person_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/visa-types/visa-on-arrival/visa_on_arrival_component.dart';

@Component(
    selector: 'visa-types-component',
    styleUrls: ['visa_types_component.css'],
    templateUrl: 'visa_types_component.html',
    directives: [
      DeferredContentDirective,
      MaterialTabPanelComponent,
      MaterialTabComponent,
      ElectronicVisaComponent,
      PaperVisaCounterComponent,
      PaperVisaPersonComponent,
      VisaOnArrivalComponent
    ])
class VisaTypesComponent {
}
