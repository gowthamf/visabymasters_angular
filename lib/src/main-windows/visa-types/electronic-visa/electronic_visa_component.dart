import 'package:angular/angular.dart';

@Component(
  selector: 'electronic-visa-component',
  styleUrls: ['electronic_visa_component.css'],
  templateUrl: 'electronic_visa_component.html',
  directives: ['MaterialButton']
)
class ElectronicVisaComponent {
  var name = 'Electronic Visa';
}
