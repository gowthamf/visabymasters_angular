import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/utils/browser/dom_service/angular_2.dart';
import 'package:angular_components/utils/browser/window/module.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/common/constants.dart';
import 'package:visabymasters_angulardart/src/common/custom-fixed-tab/custom_fixed_tab.dart';
import 'package:visabymasters_angulardart/src/main-windows/route_paths.dart';
import 'package:visabymasters_angulardart/src/main-windows/routes.dart';
import 'package:visabymasters_angulardart/src/services/country_service.dart';

@Component(
  selector: 'tab-navigation-component',
  templateUrl: 'tab_navigation_component.html',
  styleUrls: ['tab_navigation_component.css'],
  directives: [
    DeferredContentDirective,
    MaterialTabPanelComponent,
    MaterialTabComponent,
    domServiceBinding,
    rtlProvider,
    windowBindings,
    CustomFixedMaterialTabStripComponent,
    NgFor,
    routerDirectives,
    CountryService
  ],
  exports: [RoutePaths, Routes],
)
class TabNavigationComponent implements OnInit, OnDestroy {
  int tabIndex = 0;
  bool stopChange = false;
  final Router _router;
  String currentPath;
  ChangeDetectorRef _changeDetectorRef;
  CountryService _countryService;

  final click = StreamController<int>();

  @Output()
  Stream<int> get clicked => click.stream;

  TabNavigationComponent(
      this._router, this._changeDetectorRef, this._countryService);

  @override
  void ngOnInit() {
    _router.onRouteActivated.listen((onData) => {
          currentPath = onData.routePath.path,
          if (currentPath == Constants.home)
            {tabIndex = 0}
          else if (currentPath == Constants.visas)
            {tabIndex = 1}
          else if (currentPath == Constants.passports)
            {tabIndex = 2}
          else if (currentPath == Constants.legalization)
            {tabIndex = 3}
          else if (currentPath == Constants.servicedirectory)
            {tabIndex = 4}
          else if (currentPath == Constants.processstatus)
            {tabIndex = 5},
          _changeDetectorRef.markForCheck()
        });
  }

  void onTabChange(TabChangeEvent event) {
    tabIndex = event.newIndex;

    click.sink.add(1); //way of adding OutPut

    if (tabIndex == 0) {
      _router.navigate(RoutePaths.home.toUrl());
      this._countryService.isShowVisa = true;
    } else if (tabIndex == 1) {
      _router.navigate(RoutePaths.visa.toUrl());
      this._countryService.isShowVisa = true;
    } else if (tabIndex == 2) {
      _router.navigate(RoutePaths.passports.toUrl());
      this._countryService.isShowVisa = true;
    } else if (tabIndex == 3) {
      _router.navigate(RoutePaths.legalization.toUrl());
      this._countryService.isShowVisa = false;
    } else if (tabIndex == 4) {
      _router.navigate(RoutePaths.servicedirectory.toUrl());
      this._countryService.isShowVisa = false;
    } else if (tabIndex == 5) {
      _router.navigate(RoutePaths.processstatus.toUrl());
      this._countryService.isShowVisa = false;
    }
  }

  void onBeforeTabChange(TabChangeEvent event) {
    if (stopChange) {
      event.preventDefault();
    }
  }

  final tabLabels = const <String>[
    'HOME',
    'VISAS',
    'DUAL-CITIZENSHIP SERVICES',
    'LEGALIZATION',
    'SERVICE DIRECTORY',
    'PROCESS STATUS'
  ];

  @override
  void ngOnDestroy() {
    _changeDetectorRef.detach();
  }
}
