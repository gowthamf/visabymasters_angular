import 'package:angular/angular.dart';
import 'package:visabymasters_angulardart/src/common/custom-scorecard/scoreboard.dart';
import 'package:visabymasters_angulardart/src/common/custom-scorecard/scorecard.dart';
import 'package:visabymasters_angulardart/src/common/constants.dart';

@Component(
  selector: 'continent-template',
  templateUrl: 'continent_template.html',
  styleUrls: ['continent_template.css'],
  directives: [
    CustomScoreboardComponent,
    CustomScorecardComponent
  ]
)
class ContinentTemplate {
  final ScoreboardType radio = ScoreboardType.radio;
  final allCountries= Constants.allCountries;
  final asia= Constants.asia;
  final europe= Constants.europe;
  final nAmerica= Constants.nAmerica;
  final sAmerica= Constants.sAmerica;
  final africa= Constants.africa;
  final asiaPacific= Constants.asiaPacific;
  final mostVisited= Constants.mostVisited;

  void selectedContinent(String selected){
    print(selected);
  }
}
