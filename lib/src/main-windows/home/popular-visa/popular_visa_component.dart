import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:visabymasters_angulardart/src/common/custom-expansion/material_expansionpanel.dart';
import 'package:visabymasters_angulardart/src/main-windows/home/popular-visa/continent-template/continent_template.dart';

@Component(
    selector: 'popular-visa-component',
    styleUrls: ['popular_visa_component.css','package:angular_components/css/mdc_web/card/mdc-card.scss.css'],
    templateUrl: 'popular_visa_component.html',
    directives: [CustomMaterialExpansionPanel,ContinentTemplate],
    providers: [overlayBindings])
class PopularVisaComponent {}
