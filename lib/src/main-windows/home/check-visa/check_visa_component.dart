import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/model/ui/has_factory.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/common/dropdown-list/dropdown_list_component.dart';
import 'package:visabymasters_angulardart/src/models/CountryDTO.dart';

import 'check_visa_component.template.dart' as demo;

@Component(
    selector: 'check-visa-component',
    styleUrls: ['check_visa_component.css'],
    templateUrl: 'check_visa_component.html',
    directives: [CountyDropdownListComponent, NgFor],
    providers: [])
class CheckVisaComponent implements OnInit {
  bool isToCountry = true;
  String hintValue = "Select Country";

  @override
  void ngOnInit() async {}
}
