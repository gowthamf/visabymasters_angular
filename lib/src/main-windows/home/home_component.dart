// ignore_for_file: unused_import, unused_local_variable
import 'package:angular/angular.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'package:visabymasters_angulardart/src/main-windows/home/check-visa/check_visa_component.dart';
import 'package:visabymasters_angulardart/src/main-windows/home/popular-visa/popular_visa_component.dart';
import 'package:visabymasters_angulardart/src/services/country_service.dart';

@Component(
  selector: 'home-component',
  templateUrl: 'home_component.html',
  styleUrls: ['home_component.css'],
  directives: [CheckVisaComponent, coreDirectives, PopularVisaComponent],
)
class HomeComponent implements OnInit, OnDestroy {
  final CountryService _countryService;

  HomeComponent(this._countryService);

  @override
  void ngOnInit() {
    _countryService.isShowVisa = true;
  }

  @override
  void ngOnDestroy() {}
}
