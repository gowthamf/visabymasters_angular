import 'package:angular/angular.dart';
import 'package:visabymasters_angulardart/src/main-windows/visas/county-template/country_template_component.dart';

@Component(
  selector: 'visa-component',
  styleUrls: ['visa_component.css'],
  templateUrl: 'visa_component.html',
  directives: [CountryTemplateComponent]
)
class VisaComponent {
  
}
