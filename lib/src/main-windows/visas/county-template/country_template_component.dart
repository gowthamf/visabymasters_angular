import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'package:visabymasters_angulardart/src/common/constants.dart';
import 'package:visabymasters_angulardart/src/models/CountryDTO.dart';

@Component(
    selector: 'country-template-component',
    templateUrl: 'country_template_component.html',
    directives: [NgFor, MaterialButtonComponent, NgIf, coreDirectives],
    styleUrls: ['country_template_component.css'])
class CountryTemplateComponent implements OnInit, OnDestroy {
  @Input()
  CountryDTO country = CountryDTO();
  static List<CountryDTO> countryList = [];
  ChangeDetectorRef _changeDetectorRef;

  final Router _router;

  CountryTemplateComponent(this._changeDetectorRef, this._router);

  @override
  void ngOnInit() {}

  void onSelect(CountryDTO country) {
    _gotoDetails(country.countryId);
  }

  Future<NavigationResult> _gotoDetails(int id) {
    return _router.navigateByUrl('visa/$id/1');
  }

  @override
  void ngOnDestroy() {
    // countryBloc.dispose();
    _changeDetectorRef.detach();
  }
}
