import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:visabymasters_angulardart/src/services/country_service.dart';

@Component(
  selector: 'top-progress-component',
  styleUrls: ['top_progress_component.css'],
  templateUrl: 'top_progress_component.html',
  directives: [MaterialProgressComponent],
)
class TopProgressComponent implements OnInit {
  final CountryService _countryService;
  TopProgressComponent(this._countryService);
  bool isLoading = true;

  @override
  void ngOnInit() {
    _countryService.getLoading.listen((onData) => {
          if (onData) {isLoading = false}
        });
  }
}
