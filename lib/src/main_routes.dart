import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/main_routes_paths.dart';

import 'main-windows/main_windows_component.template.dart' as main_template;
import 'sign-in/sign_in_component.template.dart' as signin_template;
import 'visa_by_country/visa_by_country_component.template.dart'
    as visabycountry_template;
import 'admin-panel/admin_panel_component.template.dart'
    as admin_panel_template;
import 'apply-now/apply_now_component.template.dart' as apply_now_template;

export 'main_routes_paths.dart';

class MainRoutes {
  static final mainPage = RouteDefinition(
    routePath: MainRoutePaths.main,
    component: main_template.MainWindowsComponentNgFactory,
  );

  static final signIn = RouteDefinition(
      routePath: MainRoutePaths.signin,
      component: signin_template.SignInComponentNgFactory);

  static final visabycountry = RouteDefinition(
      routePath: MainRoutePaths.visabycountry,
      component: visabycountry_template.VisaByCountryComponentNgFactory);

  static final adminpanel = RouteDefinition(
      routePath: MainRoutePaths.adminpanel,
      component: admin_panel_template.AdminPanelComponentNgFactory);

  static final applyNow = RouteDefinition(
      routePath: MainRoutePaths.applyNow,
      component: apply_now_template.ApplyNowComponentNgFactory);

  static final all = <RouteDefinition>[
    mainPage,
    signIn,
    visabycountry,
    adminpanel,
    applyNow,
    RouteDefinition.redirect(
      path: '',
      redirectTo: MainRoutePaths.main.toUrl(),
    ),
  ];
}
