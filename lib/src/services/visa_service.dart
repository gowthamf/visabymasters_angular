import 'package:visa_by_masters_common/visa_by_masters_common.dart';

class VisaService {
  int fromCountryId = 0;
  int toCountryId = 0;
  bool isVisaCountry=false;

  void getVisaRequirements(int fromCountryId, int toCountryId) {
    visaBloc.getVisaRequirement(fromCountryId, toCountryId);
  }

  void getVisaRequirementsDetails(int visaReqId){
    visaDetailsBloc.getVisaDetails(visaReqId);
  }
}
