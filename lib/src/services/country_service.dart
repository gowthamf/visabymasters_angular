import 'dart:async';

import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'package:visabymasters_angulardart/src/models/CountryDTO.dart';

class CountryService {
  bool isLoaded = false;
  bool isShowVisa = true;
  final _loadingData = StreamController<bool>();

  Stream<bool> get getLoading => _loadingData.stream;

  void setLoading(bool isLoading) {
    _loadingData.sink.add(isLoading);
  }

  void getAllCountries() {
    countryBloc.getCountries();
  }
}
