import 'package:visabymasters_angulardart/src/models/applicant.dart';
import 'package:visabymasters_angulardart/src/models/general_info.dart';
import 'package:visabymasters_angulardart/src/models/visaTypeSection.dart';

class ApplyNowService {
  GeneralInfo generalInfo = GeneralInfo();
  VisaTypeSection visaType = VisaTypeSection();
  List<Applicant> applicantList = [Applicant()];
}
