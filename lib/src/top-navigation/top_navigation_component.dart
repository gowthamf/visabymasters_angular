import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visabymasters_angulardart/src/main-windows/tab-navigation/tab_navigation_component.dart';

@Component(
  selector: 'top-navigation',
  styleUrls: [
    'top_navigation_component.css',
    'package:angular_components/app_layout/layout.scss.css'
  ],
  templateUrl: 'top_navigation_component.html',
  directives: [
    MaterialTemporaryDrawerComponent,
    DeferredContentDirective,
    MaterialButtonComponent,
    MaterialIconComponent,
    TabNavigationComponent,
    MaterialProgressComponent
  ],
)
class TopNavigationComponent implements OnInit {
  bool customWidth = false;
  bool end = false;
  bool isLoading = true;
  Router _router;

  TopNavigationComponent(this._router);

  @override
  void ngOnInit() {}

  void showSignIn() {
    _router.navigateByUrl('sign-in', reload: true);
  }

  void navigateToHome() {
    _router.navigateByUrl('main/home', reload: true);
  }

  void navigateToApplyNow() {
    _router.navigateByUrl('apply-now', reload: true);
  }
}
