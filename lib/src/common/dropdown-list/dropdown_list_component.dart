import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'package:visabymasters_angulardart/src/models/CountryDTO.dart';
import 'package:visabymasters_angulardart/src/services/country_service.dart';
import 'package:visabymasters_angulardart/src/services/visa_service.dart';

@Component(
  selector: 'country-dropdown-list-component',
  templateUrl: 'dropdown_list_component.html',
  styleUrls: ['dropdown_list_component.css'],
  directives: [
    displayNameRendererDirective,
    FocusItemDirective,
    FocusListDirective,
    MaterialSelectComponent,
    MaterialSelectItemComponent,
    MaterialDropdownSelectComponent,
    MaterialSelectSearchboxComponent,
    NgFor,
    coreDirectives
  ],
  providers: <dynamic>[popupBindings],
)
class CountyDropdownListComponent implements OnInit, OnDestroy {
  @Input()
  String hintValue;
  ItemRenderer itemRenderer;
  @Input()
  bool isToCountry;

  final _selectedCountry = StreamController<dynamic>();

  @Output()
  Stream<dynamic> get selectedCountry => _selectedCountry.stream;

  final Router _router;
  final NgZone _ngZone;
  final ChangeDetectorRef _changeDetectorRef;

  final CountryService _countryService;
  final VisaService _visaService;

  static bool isCountryListLoaded = false;
  static bool isDeafultCountryListLoaded = false;

  static List<CountryDTO> countryList = [];
  static List<CountryDTO> defaultCountryList = [];

  ExampleSelectionOptions languageListOptions;

  SelectionModel<CountryDTO> singleSelectModel;
  CountryDTO _selectedFromCountry;
  CountryDTO _selectedToCountry;

  CountyDropdownListComponent(this._countryService, this._router,
      this._visaService, this._ngZone, this._changeDetectorRef);

  @override
  void ngOnInit() {
    if (_countryService.isShowVisa) {
      countryBloc.countryList.listen((onData) => {
            if (isToCountry == null && !isCountryListLoaded)
              {
                onData.forEach((f) => {
                      countryList.add(CountryDTO(
                          countryId: f.countryId,
                          countryName: f.countryName,
                          countryFlagPath: f.countryFlagPath,
                          isPopularVisa: f.isPopularVisa))
                    }),
                isCountryListLoaded = true,
                _countryService.setLoading(true),
              },
            languageListOptions = ExampleSelectionOptions(countryList),
            _ngZone.runAfterChangesObserved(getSelectedCountries)
          });
    } else {
      countryBloc.defaultCountryList.length.then((onValue) => {
            print(onValue),
          });
      countryBloc.defaultCountryList.listen((onData) => {
            if (!isDeafultCountryListLoaded)
              {
                onData.forEach((f) => {
                      defaultCountryList.add(CountryDTO(
                          countryId: f.countryId,
                          countryName: f.countryName,
                          countryFlagPath: f.countryFlagPath,
                          isPopularVisa: f.isPopularVisa))
                    }),
                isDeafultCountryListLoaded = true,
                _countryService.setLoading(true),
                languageListOptions =
                    ExampleSelectionOptions(defaultCountryList),
                _changeDetectorRef.markForCheck(),
              }
          });
    }

    if (isToCountry == null) {
      languageListOptions = ExampleSelectionOptions(defaultCountryList);
    } else {
      languageListOptions = ExampleSelectionOptions(countryList);
    }

    getSelectedCountries();
  }

  void getSelectedCountries() {
    if (_visaService.fromCountryId != 0 &&
        _visaService.toCountryId != 0 &&
        countryList.isNotEmpty) {
      if (isToCountry == null) {
        countryList.forEach((f) => {
              if (f.countryId == _visaService.fromCountryId)
                {_selectedFromCountry = f}
            });
        singleSelectModel =
            SelectionModel<CountryDTO>.single(selected: _selectedFromCountry);
      } else {
        countryList.forEach((f) => {
              if (f.countryId == _visaService.toCountryId)
                {
                  _selectedToCountry = f,
                }
            });
        singleSelectModel =
            SelectionModel<CountryDTO>.single(selected: _selectedToCountry);
      }
    } else {
      singleSelectModel = SelectionModel<CountryDTO>.single();
    }
  }

  static final List<RelativePosition> _popupPositionsAboveInput = const [
    RelativePosition.AdjacentTopLeft,
    RelativePosition.AdjacentTopRight
  ];
  static final List<RelativePosition> _popupPositionsBelowInput = const [
    RelativePosition.AdjacentBottomLeft,
    RelativePosition.AdjacentBottomRight
  ];

  // Specifying an itemRenderer avoids the selected item from knowing how to
  // display itself.

  bool useFactoryRenderer = false;
  bool useItemRenderer = false;
  bool useLabelFactory = false;
  bool useOptionGroup = false;
  bool withHeaderAndFooter = false;
  bool popupMatchInputWidth = true;
  bool visible = false;
  bool deselectOnActivate = true;
  String deselectLabel = 'None';

  /// Languages to choose from.

  StringSelectionOptions<CountryDTO> get languageOptions => languageListOptions;

  /// Single Selection Model

  String get singleSelectLanguageLabel {
    return singleSelectModel.selectedValues.isNotEmpty
        ? CountryDTO.itemRenderer(singleSelectModel.selectedValues.first)
        : hintValue;
  }

  final SelectionModel<int> widthSelection = SelectionModel<int>.single();
  final SelectionOptions<int> widthOptions =
      SelectionOptions<int>.fromList([0, 1, 2, 3, 4, 5]);
  String get widthButtonText => widthSelection.selectedValues.isNotEmpty
      ? widthSelection.selectedValues.first.toString()
      : '0';

  final SelectionModel<String> popupPositionSelection =
      SelectionModel<String>.single();
  final StringSelectionOptions popupPositionOptions =
      StringSelectionOptions<String>(['Auto', 'Above', 'Below']);
  String get popupPositionButtonText =>
      popupPositionSelection.selectedValues.isNotEmpty
          ? popupPositionSelection.selectedValues.first
          : 'Auto';

  final SelectionModel<String> slideSelection = SelectionModel<String>.single();
  final StringSelectionOptions slideOptions =
      StringSelectionOptions<String>(['Default', 'x', 'y']);
  String get slideButtonText => slideSelection.selectedValues.isNotEmpty
      ? slideSelection.selectedValues.first
      : 'Default';

  int get width => widthSelection.selectedValues.isNotEmpty
      ? widthSelection.selectedValues.first
      : null;

  List<RelativePosition> get preferredPositions {
    switch (popupPositionButtonText) {
      case 'Above':
        return _popupPositionsAboveInput;
      case 'Below':
        return _popupPositionsBelowInput;
    }
    return RelativePosition.overlapAlignments;
  }

  String get slide => slideSelection.selectedValues.isNotEmpty &&
          slideSelection.selectedValues.first != 'Default'
      ? slideSelection.selectedValues.first
      : null;

  String get singleSelectedLanguage =>
      singleSelectModel.selectedValues.isNotEmpty
          ? singleSelectModel.selectedValues.first.uiDisplayName
          : null;

  void onSelectionChange(CountryDTO selected) {
    if (selected != null && isToCountry == null) {
      selected.isToCountry = false;
    }
    if (selected != null && isToCountry != null) {
      selected.isToCountry = true;
    }
    if (selected != null) {
      getSelectedId(selected);
      navigateToCountry(selected);
    }

    if (selected == null && isToCountry == null) {
      resetSelection(false);
    }

    if (selected == null && isToCountry != null) {
      resetSelection(true);
    }

    _selectedCountry.sink.add(selected);
  }

  @ViewChild(MaterialSelectSearchboxComponent)
  MaterialSelectSearchboxComponent searchbox;

  void onDropdownVisibleChange(bool visible) {
    if (visible) {
      Timer.run(() {
        searchbox.focus();
      });
    }
  }

  void navigateToCountry(CountryDTO country) {
    if (_visaService.toCountryId != 0 &&
        _visaService.fromCountryId != 0 &&
        _visaService.toCountryId != _visaService.fromCountryId) {
      _router.navigateByUrl(
          'visa/${_visaService.fromCountryId}/${_visaService.toCountryId}');
    }
  }

  void getSelectedId(CountryDTO selectedId) {
    if (selectedId.isToCountry) {
      _visaService.toCountryId = selectedId.countryId;
    } else if (!selectedId.isToCountry) {
      _visaService.fromCountryId = selectedId.countryId;
    }
  }

  void resetSelection(bool reset) {
    if (reset) {
      _visaService.toCountryId = 0;
    } else {
      _visaService.fromCountryId = 0;
    }
  }

  @override
  void ngOnDestroy() {
    countryBloc.dispose();
  }
}
