class Constants {
  static String get home => 'home';
  static String get visas => 'visas';
  static String get passports => 'passports';
  static String get legalization => 'legalization';
  static String get servicedirectory => 'service-directory';
  static String get processstatus => 'process-status';

  static String get mainpage => 'main';
  static String get signin => 'sign-in';
  static String get visabycountry => 'visa';
  static String get adminpanel => 'admin-panel';
  static get applyNow => 'apply-now';

  static String get allCountries => 'ALL';
  static String get mostVisited => 'MOST VISITED';
  static String get asia => 'ASIA';
  static String get africa => 'AFRICA';
  static String get europe => 'EUROPE CIS';
  static String get asiaPacific => 'ASIA PACIFIC';
  static String get nAmerica => 'NORTH AMERICA';
  static String get sAmerica => 'SOUTH AMERICA';
}
