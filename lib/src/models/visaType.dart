class VisaType {
  final int visaTypeId;
  final String visaName;

  VisaType(this.visaTypeId, this.visaName);
}
