class OptionalServices {
  final int optionalServiceId;
  final String optionalServiceName;
  bool optionalServiceSelected;

  OptionalServices(this.optionalServiceId, this.optionalServiceName,
      this.optionalServiceSelected);
}
