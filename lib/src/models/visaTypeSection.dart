import 'package:visabymasters_angulardart/src/models/CountryDTO.dart';
import 'package:visabymasters_angulardart/src/models/optionalService.dart';
import 'package:visabymasters_angulardart/src/models/visaType.dart';

class VisaTypeSection {
  CountryDTO country;
  VisaType applyVisaType;
  OptionalServices optionalService;
}
