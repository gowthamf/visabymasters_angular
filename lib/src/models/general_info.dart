import 'package:angular_components/angular_components.dart';

class GeneralInfo {
  String emailAddress;
  String contactNumber;
  String homeAddress;
  Date depDate;
  Date arrDate;
}
