import 'package:angular_components/angular_components.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';

class CountryDTO implements HasUIDisplayName {
  final int countryId;
  final String countryName;
  final String countryFlagPath;
  bool isToCountry = true;
  final bool isPopularVisa;

  CountryDTO({this.countryId, this.countryName, this.countryFlagPath,this.isPopularVisa});

  @override
  String get uiDisplayName => countryName;

  @override
  String toString() => uiDisplayName;

  static final ItemRenderer _itemRenderer = newCachingItemRenderer<CountryDTO>(
      (country) => "${country.countryName} (${country.countryId})");

  static const bool useItemRenderer = false;

  static final ItemRenderer _displayNameRenderer =
      (item) => (item as HasUIDisplayName).uiDisplayName;

  static ItemRenderer<CountryDTO> get itemRenderer =>
      useItemRenderer ? _itemRenderer : _displayNameRenderer;
}

class ExampleSelectionOptions extends StringSelectionOptions<CountryDTO>
    implements Selectable<CountryDTO> {
  ExampleSelectionOptions(List<CountryDTO> options)
      : super(options,
            toFilterableString: (CountryDTO option) => option.toString());
  ExampleSelectionOptions.withOptionGroups(List<OptionGroup> optionGroups)
      : super.withOptionGroups(optionGroups,
            toFilterableString: (CountryDTO option) => option.toString());
  @override
  SelectableOption getSelectable(CountryDTO item) =>
      item is CountryDTO && item.countryName.contains('en')
          ? SelectableOption.Disabled
          : SelectableOption.Selectable;
}
