import 'package:angular_components/angular_components.dart';

class Applicant {
  String fName;
  String lName;
  String gender;
  String cOfBirth;
  Date dOfBirth;
  String nationality;
  String pptNo;
  Date pptExp;
  Date pptIss;
}
