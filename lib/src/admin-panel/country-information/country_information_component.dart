import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'package:visabymasters_angulardart/src/common/dropdown-list/dropdown_list_component.dart';
import 'package:visabymasters_angulardart/src/models/CountryDTO.dart';

@Component(
  selector: 'country-information-component',
  templateUrl: 'country_information_component.html',
  styleUrls: ['country_information_component.css'],
  providers: [overlayBindings],
  directives: [
    AutoFocusDirective,
    FocusListDirective,
    MaterialIconComponent,
    MaterialButtonComponent,
    MaterialExpansionPanel,
    MaterialExpansionPanelAutoDismiss,
    MaterialExpansionPanelSet,
    MaterialDialogComponent,
    MaterialInputComponent,
    materialInputDirectives,
    MaterialYesNoButtonsComponent,
    ModalComponent,
    NgFor,
    NgModel,
    MaterialCheckboxComponent,
    CountyDropdownListComponent,
    MaterialFabComponent,
    NgIf,
  ],
  preserveWhitespace: true,
)
class CountryInformationComponent implements OnInit {
  bool isSelected = false;
  List<CountryDTO> popularVisa = [];
  String hintValue = "Select Country";

  void selectPopularVisa() {
    print(isSelected);
  }

  @override
  void ngOnInit() {}
}
