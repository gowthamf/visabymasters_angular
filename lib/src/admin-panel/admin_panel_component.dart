import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'package:visabymasters_angulardart/src/admin-panel/completed-applications/completed_application_component.dart';
import 'package:visabymasters_angulardart/src/admin-panel/country-information/country_information_component.dart';
import 'package:visabymasters_angulardart/src/admin-panel/pending-applications/pending_applications_component.dart';
import 'package:visabymasters_angulardart/src/admin-panel/visa-requirements/visa_requirements_component.dart';
import 'package:visabymasters_angulardart/src/common/constants.dart';
import 'package:visabymasters_angulardart/src/services/country_service.dart';

@Component(
    selector: 'admin-panel-component',
    templateUrl: 'admin_panel_component.html',
    styleUrls: [
      'admin_panel_component.css'
    ],
    directives: [
      VisaRequirementsComponent,
      PendingApplicationsComponent,
      CountryInformationComponent,
      CompletedApplicationComponent
    ])
class AdminPanelComponent implements OnInit {
  AdminPanelComponent();

  @override
  void ngOnInit() {
    countryBloc.getDefaultCountries();
  }
}
