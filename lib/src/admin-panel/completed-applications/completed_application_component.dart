import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

@Component(
  selector: 'completed-application-component',
  templateUrl: 'completed_application_component.html',
  providers: [overlayBindings],
  directives: [
    AutoFocusDirective,
    FocusListDirective,
    MaterialIconComponent,
    MaterialButtonComponent,
    MaterialExpansionPanel,
    MaterialExpansionPanelAutoDismiss,
    MaterialExpansionPanelSet,
    MaterialDialogComponent,
    MaterialInputComponent,
    materialInputDirectives,
    MaterialYesNoButtonsComponent,
    ModalComponent,
    NgFor,
    NgModel,
  ],
  preserveWhitespace: true,
)
class CompletedApplicationComponent {
  var name = 'Angular';
}
